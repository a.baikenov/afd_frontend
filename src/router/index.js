import { createRouter, createWebHistory } from 'vue-router'
import PublicLayout from '../views/layouts/PublicLayout.vue';
import CabinetLayout from "../views/layouts/CabinetLayout.vue";

// eslint-disable-next-line no-prototype-builtins
const isAuthorized = localStorage.hasOwnProperty('token');
const authGuard = function (to, from, next){
  if(!isAuthorized) next({ name: 'login' });
  else next()
}

const routes = [

  {
    path: '/',
    component: PublicLayout,
    children: [
      {
        path: '',
        name: 'Home',
        component: () => import('../views/auth-pages/Login.vue'),
        meta:{
          activeNav:"1"
        }
      },
      {
        path: 'test',
        name: 'TestPage',
        component: () => import('../views/TestPage'),
        meta:{
          activeNav:"1"
        }
      },
      {
        path: '/login',
        name: 'login',
        component: () => import('../views/auth-pages/Login.vue'),
        meta:{
          activeNav:"2"
        }
      }
    ],
  },
  {
    path: '/cabinet',
    component: CabinetLayout,
    beforeEnter: authGuard,
    children: [
      {
        path: '',
        name: 'CabinetPage',
        component: () => import('../views/cabinet-pages/MainCabinetPage'),
        meta: {
          activeNav: "1",
        }
      },
      {
        path: 'person/all',
        name: 'PersonAllPage',
        component: () => import('../views/cabinet-pages/ClientsPage'),
        meta: {
          activeNav: "2",
          pageName: "all",
          personRole: "all",
          pageTitle: "Все",
          secondAside:[
            {
              page: 'all',
              name: 'Все',
              slug: 'PersonAllPage'
            },
            {
              page: 'client',
              name: 'Клиент',
              slug: 'ClientsPage'
            },
            {
              page: 'counterparty',
              name: 'Контрагент',
              slug: 'CounterpartyPage'
            },
          ],
        }
      },
      {
        path: 'person/client',
        name: 'ClientsPage',
        component: () => import('../views/cabinet-pages/ClientsPage'),
        meta: {
          activeNav: "2",
          pageName: "client",
          personRole: 1,
          pageTitle: "Клиенты",
          secondAside:[
            {
              page: 'all',
              name: 'Все',
              slug: 'PersonAllPage'
            },
            {
              page: 'client',
              name: 'Клиент',
              slug: 'ClientsPage'
            },
            {
              page: 'counterparty',
              name: 'Контрагент',
              slug: 'CounterpartyPage'
            },
          ],
        }
      },
      {
        path: 'person/counterparty',
        name: 'CounterpartyPage',
        component: () => import('../views/cabinet-pages/ClientsPage'),
        meta: {
          activeNav: "2",
          pageName: "counterparty",
          personRole: 2,
          pageTitle: "Контрагенты",
          secondAside:[
            {
              page: 'all',
              name: 'Все',
              slug: 'PersonAllPage'
            },
            {
              page: 'client',
              name: 'Клиент',
              slug: 'ClientsPage'
            },
            {
              page: 'counterparty',
              name: 'Контрагент',
              slug: 'CounterpartyPage'
            },
          ],
        }
      },
      {
        path: 'person/:id',
        name: 'PersonPage',
        component: () => import('../views/cabinet-pages/PersonPage'),
        meta: {
          activeNav: "2"
        }
      },
      {
        path: 'add-person',
        name: 'AddPerson',
        component: () => import('../views/cabinet-pages/AddPersonPage'),
        meta: {
          activeNav: "2",
          pageType: "add"
        }
      },
      {
        path: 'edit-person/:id',
        name: 'EditPerson',
        component: () => import('../views/cabinet-pages/AddPersonPage'),
        meta: {
          activeNav: "2",
          pageType: "edit"
        }
      },
      {
        path: 'agreements',
        name: 'AgreementsPage',
        component: () => import('../views/cabinet-pages/AgreementsPage'),
        meta: {
          activeNav: "3"
        }
      },
      {
        path: 'agreements/:id',
        name: 'AgreementInPage',
        component: () => import('../views/cabinet-pages/AgreementInPage'),
        meta: {
          activeNav: "3"
        }
      },
    ],
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
