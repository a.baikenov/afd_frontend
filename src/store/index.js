import { createStore } from 'vuex'

// Modules
import AuthModule from './modules/AuthModule'
import PersonModule from './modules/PersonModule'

export default createStore({
  modules: {
    AuthModule,
    PersonModule
  }
})
