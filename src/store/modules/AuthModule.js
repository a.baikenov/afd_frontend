import {DefaultAPIInstance, LoginAPIInstance} from "@/plugins/axios";
import router from "@/router";
export const ON_LOGIN = "ON_LOGIN";
export const ON_LOGOUT = "ON_LOGOUT";
export const SET_TOKEN = "SET_TOKEN";
export const SET_REFRESH_TOKEN = "SET_REFRESH_TOKEN";
export const SET_USER_INFO = "SET_USER_INFO";
export const DELETE_TOKEN = "DELETE_TOKEN";
export const DELETE_USER_INFO = "DELETE_USER_INFO";

const state = {
    credentials: {
        token: localStorage.getItem('token') || null,
        refresh_token: localStorage.getItem('refresh_token') || null,
        userInfo: localStorage.getItem('userInfo') || null
    }
}

const actions = {
    [ON_LOGIN]: async (store, form) => {
        const { data }  = await LoginAPIInstance({url:'/auth/login',method:'POST', data:form});
        return data;
    },
    [ON_LOGOUT]: async (store) => {
        store.commit(DELETE_TOKEN);
        store.commit(DELETE_USER_INFO);
        delete DefaultAPIInstance.defaults.headers['authorization'];
        router.push({name: 'Login'});
    }
}

const mutations = {
    [SET_TOKEN]: (state, token) => {
        state.credentials.token = token;
        localStorage.setItem('token', token);
    },
    [SET_REFRESH_TOKEN]: (state, token) => {
        state.credentials.refresh_token = token;
        localStorage.setItem('refresh_token', token);
    },
    [SET_USER_INFO]: (state, userInfo) => {
        state.credentials.userInfo = userInfo;
        localStorage.setItem('userInfo', userInfo);
    },
    [DELETE_TOKEN]: (state) => {
        state.credentials.token = null;
        localStorage.removeItem('token');
        state.credentials.refresh_token = null;
        localStorage.removeItem('refresh_token');
    },
    [DELETE_USER_INFO]: (state) => {
        state.credentials.userInfo = null;
        localStorage.removeItem('userInfo');
    }
}

const getters = {
    get_user_info: (state) => state.credentials.userInfo
}
export default {
    state,
    actions,
    mutations,
    getters
}
