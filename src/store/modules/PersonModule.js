import {DefaultAPIInstance} from "@/plugins/axios";
export const GET_PERSONS = "GET_PERSONS";
export const GET_PERSON_BY_ID = "GET_PERSON_BY_ID";
export const GET_PERSON_BY_ID_FOR_EDIT = "GET_PERSON_BY_ID_FOR_EDIT";
export const GET_DICTIONARIES = "GET_DICTIONARIES";
export const SET_DICTIONARIES = "SET_DICTIONARIES";
export const ADD_PERSON = "ADD_PERSON";
export const EDIT_PERSON = "EDIT_PERSON";
export const DELETE_PERSON = "DELETE_PERSON";
export const PERSON_FORM_DEFAULT = "PERSON_FORM_DEFAULT";
export const PERSON_FORM_UPDATE = "PERSON_FORM_UPDATE";

const state = {
    personForm: '',
    dictionaries: '',
    options:{
        boolean: [
            {
                "id": true,
                "name": "Да"
            },
            {
                "id": false,
                "name": "Нет"
            }
        ],
        rules:{
            required_input:{
                required: true,
                message: 'Обязательное поле'
            },
        },
    }
}

const actions = {
    [GET_PERSONS]: async (store, role_id) => {
        let person_role = '';
        if(role_id !== "all"){
            person_role = '&person_role_id='+role_id
        }
        const { data }  = await DefaultAPIInstance({url:'/person?limit=10'+person_role,method:'GET'});
        return data;
    },
    [GET_PERSON_BY_ID]: async (store, person_id) => {
        const { data }  = await DefaultAPIInstance({url:'/person/'+person_id,method:'GET'});
        return data;
    },
    [GET_PERSON_BY_ID_FOR_EDIT]: async (store, person_id) => {
        const { data }  = await DefaultAPIInstance({url:'/person/'+person_id,method:'GET'});
        store.commit('PERSON_FORM_UPDATE', data.data);
        return data;
    },
    [GET_DICTIONARIES]: async (store) => {
        const { data }  = await DefaultAPIInstance({url:'/dictionaries',method:'GET'});
        store.commit('SET_DICTIONARIES',data.data)
    },
    [ADD_PERSON]: async (store) => {
        let form = store.state.personForm;
        if(form.type_id === 1){
            delete form.ip;
            delete form.company;
        }
        if(form.type_id === 2){
            delete form.people;
            delete form.company;
        }
        if(form.type_id === 3){
            delete form.people;
            delete form.ip;
        }
        const { data }  = await DefaultAPIInstance({url:'/person',method:'POST',data:form});
        return data;
    },
    [EDIT_PERSON]: async (store) => {
        let form = store.state.personForm;
        if(form.type_id === 1){
            delete form.ip;
            delete form.company;
        }
        if(form.type_id === 2){
            delete form.people;
            delete form.company;
        }
        if(form.type_id === 3){
            delete form.people;
            delete form.ip;
        }
        const { data }  = await DefaultAPIInstance({url:'/person/'+form.id,method:'PUT',data:form});
        return data;
    },
    [DELETE_PERSON]: async (store, person_id) => {
        const { data }  = await DefaultAPIInstance({url:'/person/'+person_id,method:'DELETE'});
        return data;
    },
}

const mutations = {
    [SET_DICTIONARIES]: (state, data) => {
        state.dictionaries = data;
    },
    [PERSON_FORM_DEFAULT]: (state) => {
        state.personForm = {
            type_id: 1,
            osnovn_code_client_1c:'',
            mfo:'',
            mfo_8:'',
            nalog_info:{
                type_id:1,
                certificate_number:'',
                certificate_start_date:'',
                certificate_end_date:''
            },
            addresses:[
                {
                    full_name:'',
                    index:'',
                    region_name:'',
                    city:'',
                    street:'',
                    house_numb:'',
                    apartment_number:'',
                    oktmo:'',
                    okato:'',
                    okato2:'',
                    comment:'',
                    type_ids:[
                        {
                            type_id: ''
                        }
                    ],
                    country_id: ''
                },
            ],
            risks:[
                {
                    risk_lvl_id: '',
                    date: '',
                    comment: ''
                },
            ],
            documents:[
                {
                    type_id:1,
                    name:'',
                    text:'',
                    comment:'',
                    series:'',
                    numb:'',
                    date:'',
                    scan_date:'',
                    start_date:'',
                    end_date:'',
                    kem_vudan:'',
                    kem_vudan_cod:'',
                    strana_vudachi_id:''
                }
            ],
            role_ids:[
                {
                    role_id:''
                }
            ],
            birzha_ids:[
                {
                    birzha_id: ''
                },
            ],
            contact:{
                site:'',
                edo:false,
                edo_date:'',
                comment:'',
                phones:[
                    {
                        number: '',
                        type_id: ''
                    }
                ],
                emails:[
                    {
                        email: '',
                        type_id: ''
                    }
                ],
            },
            licenses:[
                {
                    vid_deyatelnosti: '',
                    nomer_licenzii: '',
                    start_date: '',
                    end_date: '',
                    kem_vydana: ''
                },
            ],
            people:{
                last_name:'',
                last_name_eng:'',
                first_name:'',
                first_name_eng:'',
                patronymic:'',
                patronymic_eng:'',
                birth_date:'',
                death_date:'',
                inn:'',
                tin:'',
                snils:'',
                podpisant:false,
                position:'',
                torg_recomend:true,
                pol_id:1
            },
            company:{
                full_international_name:'',
                short_international_name:'',
                inn:'',
                tin:'',
                ogrn:'',
                bik:'',
                data_reg:'',
                organ_registr:'',
                kpp:'',
                full_name_comp:'',
                short_name_comp:'',
                ustav_capital: 0,
                okpo:'',
                okonh:'',
                okfc:'',
                okved:'',
                name_comp_for_cb:'',
                nomin_derj:false,
                inostr_nomin_derj:false,
                okogu:'',
                oktmo:'',
                credit_comp:false,
                sektor_ekon_id:1
            },
            ip:{
                name_comp_for_cb:'',
                data_reg:'',
                ogrn:'',
                organ_registr:'',
                okpo:'',
                okonh:'',
                okfc:'',
                okved:'',
                okogu:'',
                oktmo:'',
                sektor_ekon_id:1
            }
        };
    },
    [PERSON_FORM_UPDATE]: (state, data) => {
        state.personForm.id = data.id;
        state.personForm.type_id = data.type_id?data.type_id:state.personForm.type_id;
        state.personForm.osnovn_code_client_1c = data.osnovn_code_client_1c?data.osnovn_code_client_1c:state.personForm.osnovn_code_client_1c;
        state.personForm.mfo = data.mfo?data.mfo:state.personForm.mfo;
        state.personForm.mfo_8 = data.mfo_8?data.mfo_8:state.personForm.mfo_8;
        state.personForm.nalog_info = data.nalog_info?data.nalog_info:state.personForm.nalog_info;
        // state.personForm.addresses = data.addresses?data.addresses:state.personForm.addresses;
        if(data.addresses && data.addresses.length>0){
            state.personForm.addresses = data.addresses;
            state.personForm.addresses.forEach(function(element) {
                element.type_ids = [{type_id:element.types[0].id}];
            });
        }
        if(data.roles && data.roles.length>0){
            state.personForm.role_ids = data.roles;
            state.personForm.role_ids.forEach(function(element) {
                element.role_id = element.id;
            });
        }
        if(data.birzhas && data.birzhas.length>0){
            state.personForm.birzha_ids = data.birzhas;
            state.personForm.birzha_ids.forEach(function(element) {
                element.birzha_id = element.id;
            });
        }
        state.personForm.risks = data.risks?data.risks:state.personForm.risks;
        state.personForm.documents = data.documents?data.documents:state.personForm.documents;
        state.personForm.contact = data.contact?data.contact:state.personForm.contact;
        state.personForm.licenses = data.licenses?data.licenses:state.personForm.licenses;
        state.personForm.people = data.people?data.people:state.personForm.people;
        state.personForm.company = data.company?data.company:state.personForm.company;
        state.personForm.ip = data.ip?data.ip:state.personForm.ip;
    },
}

const getters = {
    get_person_form: (state) => state.personForm,
    get_dictionaries: (state) => state.dictionaries,
    get_options: (state) => state.options,
}
export default {
    state,
    actions,
    mutations,
    getters
}
