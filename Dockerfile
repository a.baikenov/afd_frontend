FROM node:gallium-alpine

ENV TZ=Asia/Almaty

WORKDIR /app

COPY package.json package-lock.json .

RUN npm install

COPY . .

RUN npm run build
